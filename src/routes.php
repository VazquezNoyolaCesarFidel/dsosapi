<?php
use Slim\Http\Request;
use Slim\Http\Response;


$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
$app->get('/entidades/',function(Request $request,Response $response,$args){
  $sql = "select * from entidades";
  try {
    $db = new db();
    $db = $db->conectar();
    $ejecutar = $db->query($sql);
    $entidades = $ejecutar->fetchAll(PDO::FETCH_OBJ);
    $db = null;
    return $response -> write (json_encode($entidades))
                     -> withHeader('Content-type','application/json');
  } catch (PDOException $e) {
    echo '{"error:{"text": '.$e->getMessage().'}"}';
  }

});
$app->group('/entidades/',function(){
  $this->get('{id}/establecimientos',function(Request $request, Response $response, $args){
    return "Entidad con id " . $args['id'];
  });


});


/*$this->get('{id}/establecimientos',function(Request $request, Response $response, $args){
  return "Entidad con id " . $args['id'];
});
$this->get('getAll', function (Request $request, Response $response,$args) {
  $usuarios = [
    ['Nombre'=>"Cesar",'Apellido' => 'Vazquez'],
    ['Nombre'=>"Wilber",'Apellido'=>'Mayren']];
  return $response->write(json_encode($usuarios))
                  ->withHeader('Content-type', 'application/json');;
});
$this->get('get/{id}',function (Request $request,Response $response,$args){
  return "Se ha seleccionado el usuario con el id " . $args['id'];
});
$this->get('get/{id}/materias', function(Request $request,Response $response, $args){
  return "Se visualizan las materias del usuario con id " . $args['id'];
});
$this->post('insertar', function(Request $request,Response $response, $args){
  $datos = $request->getParsedBody();
  return $datos['nombre'] . " " . $datos['apellido'];
});*/
